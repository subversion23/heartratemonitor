﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace HeartRateMonitor
{

    [Service]
    class HRService : Service
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        IBinder mBinder;
        static readonly string TAG = "X:" + typeof(HRService).Name;

        public HRService()
        {
            
        }

        public void Stop()
        {
            StopSelf();
        }

        public override void OnCreate()
        {
            base.OnCreate();

            if (Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.O)
            {
                StartForeground(23, new Notification());
            }
            else
            {
                StartService(new Intent());
            }
            Logger.Debug(TAG, "Service Created");

        }

        public override void OnDestroy()
        {

            Logger.Debug(TAG, "Service destroyed");
            base.OnDestroy();
        }

        public override IBinder OnBind(Intent intent)
        {
            // Return null because this is a pure started service. A hybrid service would return a binder that would
            // allow access to the GetFormattedStamp() method.
            return null;
        }
    }
}