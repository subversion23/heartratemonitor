﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SQLite;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using System.Threading.Tasks;
using System.Net.Http;
using System.ComponentModel;

namespace HeartRateMonitor
{

 
    [Table("hrdata")]
    public class HRData
    {
        public DateTime Time { get; set; }
        public int HeartRate { get; set; }
        public string RRS { get; set; }

        public HRData()
        {

        }

        public HRData(DateTime time, int hr, List<int> rrs)
        {
            Time = time;
            HeartRate = hr;
            foreach (int i in rrs)
            {
                RRS += i.ToString() + " ";
            }
            if (RRS != null)
                RRS = RRS.Trim();

            //var srrs = rrs.Select(r => r.ToString() + " "); Wieso gibt das System.Linq.Enumerable+SelectListIterator`2[System.Int32,System.String] ?????????

            //RRS = srrs.ToString();

        }


        //[Ignore]
        //public int MyHeartRate
        //{
        //    get
        //    {
        //        return 1000 / LastRRS * 60;
        //    }
        //}
        //[Ignore]
        //int LastRRS
        //{
        //    get
        //    {
        //        string rrs = RRS.Trim().Split(" ").LastOrDefault(x => x.Length > 0);
        //        Console.WriteLine(rrs);
        //        return int.Parse(rrs);
        //    }
        //}

        public class Location
        {
            public DateTime Time { get; set; }
            public int AreaID { get; set; }
        }
        public class Area
        {
            [PrimaryKey, AutoIncrement]
            public int ID { get; set; }
            public string Name { get; set; }
            public string WifiName { get; set; }
            // .. Coordinates
        }
        public class Comments
        {
            [PrimaryKey,AutoIncrement]
            public int ID { get; set; }
            public DateTime Time { get; set; }
            public string Text;
        }

    }

    public class HRDatabase
    {
        private List<HRData> HRDataCache; 
        public int CacheSize = 40;

        public SQLiteConnection connection;
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public HRDatabase()
        {        
            string path = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDocuments).AbsolutePath;
            path = Path.Combine(path, "polarh7data.db");
            connection = new SQLiteConnection(path);
            CreateTable();
            HRDataCache = new List<HRData>();
            //var t = ExportAllToArchimedes();
            System.Diagnostics.Debug.Print("DB Path: " + path);

        }
        public void CreateTable()
        {
            var result = connection.CreateTable<HRData>();
            if (result == CreateTableResult.Created || result == CreateTableResult.Migrated)
                Logger.Warn("Table Changed. " + result.ToString());

        }

        public void AddHRData(HRData data)
        {
            HRDataCache.Add(data);            
            if (HRDataCache.Count >= CacheSize)
                CommitCache();
        }
        public void CommitCache()
        {
            if (HRDataCache.Count > 0)
            {
                connection.InsertAll(HRDataCache);
                connection.Commit();
                HRDataCache.Clear();
            }
        }

        public void InsertToDB(HRData data)
        {
            //HRData m = new HRData(DateTime.Now,data.HeartRate,data.rrs);

            //m.Time = DateTime.Now;
            //m.HeartRate = data.HeartRate;
            //string rrs = "";
            //foreach (int i in data.rrs)
            //{                
            //    rrs += i.ToString() + " ";
            //}
            //m.RRS = rrs;

            try
            {
                connection.Insert(data);
                connection.Commit();

            }
            catch (Exception ex)
            {
                //this.Close();
                Logger.Error(ex, "Shit");
                throw;
            }

        }

        public async Task<string> ExportLatest()
        {
            CommitCache();
            Exporter exporter = new Exporter();
            var latest = await exporter.LastExportDate();   
            
            var data = connection.Table<HRData>().Where(d => d.Time > latest).ToList();
            var result = await exporter.SendToArchimedes(data);
            return result;
            
        }

        public void Close()
        {
            connection.Commit();
            connection.Close();
        }
    }

    class Exporter
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        HttpClient Client;
        //readonly string Host = "http://192.168.1.2:23023";
        readonly string Host = "http://archimedes:23023";

        public Exporter()
        {
            Client = new HttpClient();
            Client.Timeout = TimeSpan.FromSeconds(5);                
        }

        public async Task<DateTime> LastExportDate()
        {
            string url = Host + "/last";
            Task<HttpResponseMessage> response = Client.GetAsync(url);
            if (response.Result.StatusCode == System.Net.HttpStatusCode.OK)
            {
                var ts = await response.Result.Content.ReadAsStringAsync(); // Datetime comes as Unixtime
                int unixtime = int.Parse(ts);
                DateTime origin = new DateTime(1970, 1, 1, 0, 0, 0, 0);
                var dt = origin.AddSeconds(unixtime);
                return dt;
            }
            else
            {
                return DateTime.MinValue;
            }          

        }
        
        public async Task<string> SendToArchimedes(List<HRData> data)
        {           

            StringBuilder sbData = new StringBuilder();
            foreach (HRData item in data)
            {
                if (item.Time.ToBinary() == 0)
                    continue;

                StringBuilder row = new StringBuilder();
                row.Append(item.Time.ToBinary()).Append(",");
                row.Append(item.HeartRate).Append(",");
                row.Append(item.RRS).Append(";");
                sbData.Append(row.ToString().Trim());
            }

            System.Net.Http.StringContent content = new StringContent(sbData.ToString());

            Console.WriteLine("Sending Data...");
            HttpResponseMessage resp;
            try
            {
                resp = await Client.PostAsync(Host, content);
                return resp.RequestMessage.ToString();
                //return resp;
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                //throw ex;
                return ex.Message;
                
            }
            
        }

        public async Task<string> SendFileToArchimedes(string filepath)
        {
            //byte[] data;
            string fileContent;
            using (var reader = new System.IO.StreamReader(filepath)) 
            {
                fileContent = await reader.ReadToEndAsync();
            }

            //System.Net.Http.ByteArrayContent bContent = new ByteArrayContent(data);
            
            StringContent strContent = new StringContent(fileContent);
            Task<HttpResponseMessage> response;
            try
            {
                response = Client.PutAsync(Host, strContent);
            }
            catch (Exception ex)
            {
                throw ex;
            }         

            return response.Result.StatusCode.ToString();

        }
    }


}