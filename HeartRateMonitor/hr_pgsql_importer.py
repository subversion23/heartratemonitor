
import psycopg2
from datetime import datetime
from http.server import HTTPServer
from http.server import BaseHTTPRequestHandler
import threading
import queue
import json
import datetime
from datetime import timezone

def log(msg):
    print(str(datetime.datetime.now()) + ": " + str(msg))

def db_import(string):   
    try:
        conn = psycopg2.connect(host="archimedes",database="wu", user="wu", password="1801")
        cur = conn.cursor()
    except psycopg2.Error as err:
        log("DB Connection Error: "+err.args[0])
        return
    
    rows = string.split(";")
        
    query = "insert into watchmen.polar7_data(time,hrate,rrs) values(%s,%s,%s) ON CONFLICT DO NOTHING;"
    counter = 0
    data = ()
    ok = True
    try:
        for r in rows:
            if r == "": # letztes elem ist leer.
                break
            
            data = tuple(r.strip().split(","))
            try:
                rrs = data[2].split(" ")
            except IndexError:
                log("IndexError beim import bei:"+ str(r))
                print (data)
                ok = False
                continue
            
            if '' in rrs:
                rrs.remove('')
            
            if len(rrs) == 0:
                rrs = []
            else:
                rrs = [int(e) for e in rrs]
            
            dt = int(data[0])
            dt = datetime.datetime.utcfromtimestamp(dt/10000000 - 62135596800)#.replace(tzinfo=wien)
            data = (dt,int(data[1]),rrs)
            cur.execute(query,data)
            counter += 1
            if counter % 1000 == 0:
                conn.commit()
                print("{0} von {1} Werten eingetragen.".format(counter,len(rows)))
                print (dt)
    
    except psycopg2.Error as err:
        log("DB error: "+str(err.args))
        log(data)
        raise        
    except Exception as ex:
        log(data)
        log("General Error. " + str(ex))
        raise
        
    conn.commit()
    conn.close()
    log("Import Abgeschlossen")
    log("{0} von {1} Werten eingetragen.".format(counter,len(rows)-1))
    return ok
        
def get_latest_db_entry():
    try:
        conn = psycopg2.connect(host="archimedes",database="wu", user="wu", password="1801")
        cur = conn.cursor()
    except psycopg2.Error as err:
        log("DB Connection Error: "+err.args[0])
        return 0

    query = "select time from watchmen.polar7_data order by time desc limit 1;"
    cur.execute(query)
    result = cur.fetchone()[0]
    conn.close()
    return result


class Server(BaseHTTPRequestHandler):
    # Get latest BAckup Entry Date
    def do_GET(self):        
        log("GET recvieved.")
        self.send_response(200)
        log(self.path)
        params = self.path.replace("/","")
      
        if params == "last":
            lastdt = get_latest_db_entry()
            lastdt = str(int(lastdt.replace(tzinfo=timezone.utc).timestamp())) # Unix time to string
            print(lastdt)
            self.send_header('Content-type','text/html')
            self.end_headers()
		    #Send the date
            self.wfile.write(lastdt.encode("utf-8"))
        else:
            self.wfile.write("Wrong Request!".encode("utf-8"))
        return
        
    def do_POST(self):
        print("POST request recieved")
        #self.send_response(100)
        #self.send_response_only(100)
        try:
            header_length = int(self.headers['content-length'])
            #print ("Header Lenght: " + str(header_length) )
            payload = self.rfile.read(header_length)
        except Exception as ex:
            msg = "Error in do_post"+ str(ex)
            self.send_error(400,msg)
            log(msg)
            #raise ex
            return

        try:
             ok = db_import(payload.decode())
        except Exception as ex:
            msg = "Your data is shit!"
            log(msg + str(ex.args[0]))
            self.send_error(400,msg)            
            #raise ex
            return 
        
        if ok:
            self.send_response(200,"paast");
            self.end_headers() # <- Wichtig! sonst keine response
            print ("send ok response")
        else:
            self.send_error(418,"(some) data was not ok")
        
        #self.end_headers()
        print ("Request finished")
        
        
    def do_PUT(self):
        log("PUT recvieved.")
        self.send_response(200)
        
        
        

#db_import(test_str)

httpd = HTTPServer(("",23023),Server)
log("Starte http Server...")
try:    
    httpd.serve_forever()
except Exception as ex:
    log ("ex und ENDE" + str(ex))
    httpd.shutdown()
    httpd.server_close()

except KeyboardInterrupt:
    log ("KeyBoard Int.")
    httpd.shutdown()
    httpd.server_close()


class DBHelper():
    def __init__(self):
        self.conn = None
        self.cur = None

    def ExecuteQuery(self,query,data=None):

        if type(data) != tuple:
            data = (data,)
        try:
            self.cur.execute(query,data)
        except psycopg2.Error as err:
            print ("DB error: "+err.args[0])
            #WriteLog ("DB error: "+err.args[0],log_fh)
            #self.conn.close()
        return self.cur.fetchall()


    def Open(self):
        try:
            self.conn = psycopg2.connect(host="archimedes",database="wu", user="wu", password="1801")
            self.cur = self.conn.cursor()
        except psycopg2.Error as err:
            print ("DB error: "+err.args[0])

    def Close(self):
        self.conn.commit()
        self.conn.close()

    def Commit(self):
        self.conn.commit()


class Listener(threading.Thread):
    # override
    def __init__(self,adress="", port=4000,bucket=None):
        self.exbucket = bucket
        try:
            super(Listener,self).__init__()
            self.httpd = HTTPServer((adress,port),WebHook)
            self.data = None
        except Exception as ex:
            if self.exbucket:
                self.exbucket.put(ex)

    # override
    def run(self):
        try:
            self.httpd.serve_forever()
        except Exception as ex:
            if self.exbucket:
                self.exbucket.put(ex)
            else:
                print ("Error in Listener:" + str(ex.args))

    #def get_doc(self):
    #    if _dispatched_docs.not_empty:
    #        doc = _dispatched_docs.get()
    #        return doc
    #    else:
    #        return _dispatched_docs.empty

    def stop(self):
        self.httpd.shutdown()
        self.httpd.server_close()
        del self.httpd
        self.join(0.2)
