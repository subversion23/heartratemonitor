﻿using System;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;
using System.Threading;
using Android.Graphics;
using Android.Support.V4.Content;
using Android;
using Android.Content.PM;
using Android.Net.Wifi;

namespace HeartRateMonitor
{ 
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        PolarH7 Polar;
        Button Button1;
        TextView ctlInfoPanel;
        TextView ctlDeviceStatus;
        TextView ctlDevice;
        HRDatabase Database;

        System.Timers.Timer CheckExportTimer;

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        protected override async void OnCreate(Bundle savedInstanceState)
        {
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.WriteExternalStorage) != Permission.Granted)
            {
                RequestPermissions(new string[] { Manifest.Permission.WriteExternalStorage }, 1);
            }
            if (ContextCompat.CheckSelfPermission(this, Manifest.Permission.ReadExternalStorage) != Permission.Granted)
            {
                RequestPermissions(new string[] { Manifest.Permission.ReadExternalStorage }, 2);
            }

            CheckExportTimer = new System.Timers.Timer(1000 * 60 *60);
            CheckExportTimer.Elapsed += CheckExportTimer_Elapsed;
            //CheckExportTimer.Start();


            WifiManager wifiManager = (WifiManager)(Application.Context.GetSystemService("wifi"));
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.activity_main);

            Android.Support.V7.Widget.Toolbar toolbar = FindViewById<Android.Support.V7.Widget.Toolbar>(Resource.Id.toolbar);
            SetSupportActionBar(toolbar);

            Button1 = FindViewById<Button>(Resource.Id.button1);
            ctlDeviceStatus = FindViewById<TextView>(Resource.Id.ctldeviceStatus);
            ctlInfoPanel = FindViewById<TextView>(Resource.Id.ctlinfopanel);
            ctlDevice = FindViewById<TextView>(Resource.Id.hrdevice);

            Button1.Click += Button_Click;
            Polar = new PolarH7();
            Logger.Info("Programm OnCreate.");        
            
            Button1.Text = "Start";    

            Polar.PolarCallback.HRDataUpdate += PolarCallback_HRDataUpdate;
            
            Polar.PolarStatusUpdate += Polar_PolarStatusUpdate;

            Database = new HRDatabase();

            System.Diagnostics.Debug.Print("DAta Count: " + Database.connection.Table<HRData>().Count().ToString());                        
            System.Diagnostics.Debug.Print("Hi, i'm running on Thread " + Thread.CurrentThread.ToString());

            //var export = await Database.ExportLatest();
            //if (export.Contains("paast"))
            //    Logger.Info("Export durchgeführt.");
            //System.Diagnostics.Debug.Print(export);
            Logger.Info(AppDomain.CurrentDomain.BaseDirectory);
        }

        protected override void OnDestroy()
        {
            Polar.Close();
            Database.Close();
            base.OnDestroy();
        }

        private void CheckExportTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            string wifi = GetLocation();
            if ( wifi == "\"wu\"")
            {                
                var t = Database.ExportLatest();
                t.Wait();
                if (t.Exception != null)
                {
                    // TODO this does not work
                    Logger.Error(t.Exception);
                }
            }
        }

        private string GetLocation()
        {
            WifiManager wifiManager = (WifiManager)(Application.Context.GetSystemService("wifi"));            
            return wifiManager.ConnectionInfo.SSID.ToString();

        }

        private void Polar_PolarStatusUpdate(object sender, PolarStatusEventArgs e)
        {
            RunOnUiThread(() =>
            {
                if (e.gattDevice != null)
                    ctlDevice.Text = e.gattDevice.Device.Name;

                ctlDeviceStatus.Text = e.newState.ToString();
                if (e.newState == HRDeviceStates.Disconnected)
                    ctlDeviceStatus.SetBackgroundColor(Color.Red);
                if (e.newState == HRDeviceStates.Connected)
                    ctlDeviceStatus.SetBackgroundColor(Color.Orange);
                if (e.newState == HRDeviceStates.ServicesFound)
                    ctlDeviceStatus.SetBackgroundColor(Color.Yellow);
                if (e.newState == HRDeviceStates.Transfering)
                    ctlDeviceStatus.SetBackgroundColor(Color.Green);
            });
        }

        private void PolarCallback_HRDataUpdate(object sender, HRDataEventArgs e)
        {
            RunOnUiThread(() =>
            {
                ctlInfoPanel.Text = e.Data.HeartRate.ToString();
                Database.AddHRData(e.Data); 
                //Database.InsertToDB(e.Data);
            });            
        }

        private void Button_Click(object sender, EventArgs e)
        {
            if (Polar.PolarStatus == HRDeviceStates.Disconnected)
            {
                Polar.ConnectGatt(this.ApplicationContext);                            

            }
            else if (Polar.PolarStatus == HRDeviceStates.Connected)
            {
                Polar.DiscoverSerives();
            }
            else if (Polar.PolarStatus == HRDeviceStates.ServicesFound)
            {
                try
                {
                    Polar.Start();
                }
                catch (Exception ex)                {
                    
                    Toast.MakeText(ApplicationContext, ex.Message, ToastLength.Long).Show();
                }

            }
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            return true;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            int id = item.ItemId;
            if (id == Resource.Id.action_settings)
            {
                return true;
            }

            return base.OnOptionsItemSelected(item);
        }
	}

    class Helper
    {

    }
        

}

