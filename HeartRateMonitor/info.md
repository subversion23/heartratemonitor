﻿# Meta
https://stackoverflow.com/questions/26760798/xamarin-android-turn-off-mono-logs

# Tests Curl
curl -D "213" -H "Content-Type: application/x-www-form-urlencoded" -X POST http://localhost:23023/

# DB

adb pull /storage/emulated/0/Documents/polar7data.db "D:\Users\wu\Documents\Visual Studio 2017\Projects\HeartRateMonitor"
# SQL Big to datetime
select datetime(Time/10000000 - 62135596800, 'unixepoch'), HeartRate, RRS from hrdata;

# postgres

SELECT "time", hrate, rrs
	FROM watchmen.polar7_data where "time" > now() - INTERVAL '1 DAY' AND hrate < 60;


SELECT to_timestamp("time"/10000000 - 62135596800), hrate, rrs
	FROM watchmen.polar7_data;


