﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Bluetooth;
using Java.Util;
using Android.Bluetooth.LE;

namespace HeartRateMonitor
{
    public class PolarH7
    {

        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        BluetoothAdapter BTAdapter;

        BluetoothDevice PolarDevice;        
        BluetoothGatt gatt = null;
        public readonly PolarCallBack PolarCallback = new PolarCallBack();

        UUID HR_MEASUREMENT = UUID.FromString("00002a37-0000-1000-8000-00805f9b34fb");
        UUID DESCRIPTOR_CCC = UUID.FromString("00002902-0000-1000-8000-00805f9b34fb");
        UUID HR_SERVICE = UUID.FromString("0000180D-0000-1000-8000-00805f9b34fb");
        public HRDeviceStates PolarStatus;
        public event PolarStatusEventHandler PolarStatusUpdate;
        
        public PolarH7()
        {
            PolarCallback.HRDataUpdate += Polarcb_HRDataUpdate;
            PolarCallback.StatusUpdate += Polarcb_StatusUpdate;

            BTAdapter = BluetoothAdapter.DefaultAdapter;
            if (!BTAdapter.IsEnabled)
                throw new Exception("Bluetooth is OFF!");

            PolarStatus = HRDeviceStates.NotBonded;

            Logger.Debug("BT Adapter erstellt.");
            if (BTAdapter.BondedDevices.Count > 0)
            {
                foreach (BluetoothDevice d in BTAdapter.BondedDevices)
                {
                    PolarDevice = d;
                    Logger.Info("PolarH7 gefunden.");
                    PolarStatus = HRDeviceStates.Disconnected;
                }
            }
        }

        public void ConnectGatt(Context context)
        {
            try
            {
                gatt = PolarDevice.ConnectGatt(context, true, PolarCallback);
                Logger.Info("Connect to Polar gatt...");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void DiscoverSerives()
        {
            gatt.DiscoverServices();
            PolarStatus = HRDeviceStates.Discovering;
            //gatt.Wait(5000);
        }
        
        public void Start()
        {
            if (gatt.Services.Count == 0)
            {
                Logger.Error("Start:No Gatt Services found, cant set notification..");
                return;
            }
            
            foreach (BluetoothGattService service in gatt.Services)
            {
                foreach (BluetoothGattCharacteristic ch in service.Characteristics)
                {
                    if (ch.Uuid.Equals(HR_MEASUREMENT))
                    {
                        if (gatt.SetCharacteristicNotification(ch, true))
                        {                            
                            PolarStatusUpdate(this, new PolarStatusEventArgs(HRDeviceStates.Transfering, gatt));
                        }
                        else
                        {
                            throw new Exception("SetCharacteristicNotification hinig");
                        }
                    }
                }
            }
        }

        public void Stop()
        {
            // TODO how to stop?
            //gatt.Close();
            gatt.Disconnect();
            //PolarDevice = null;
        }

        public void Close()
        {
            Stop();
            //TODO
        }

        protected void OnStatusUpdate(PolarStatusEventArgs e)
        {            
            PolarStatusUpdate(this, e);
        }

        private void Polarcb_StatusUpdate(object sender, GattStatusEventArgs e)
        {
            if (e.msg == "Connected")
            {
                PolarStatus = HRDeviceStates.Connected;
                
            }
            else if (e.msg == "Disconnected")
            {
                PolarStatus = HRDeviceStates.Disconnected;
                
            }
            else if (e.msg == "ServicesFound")
            {                
                PolarStatus = HRDeviceStates.ServicesFound;
            }
            OnStatusUpdate(new PolarStatusEventArgs(PolarStatus, e.gatt));
        }

        private void Polarcb_HRDataUpdate(object sender, HRDataEventArgs e)
        {
            //string hr = e.Data.HeartRate.ToString();
            if (PolarStatus != HRDeviceStates.Transfering)
            {
                PolarStatus = HRDeviceStates.Transfering;
                OnStatusUpdate(new PolarStatusEventArgs(HRDeviceStates.Transfering, null));
            }            
        }
    }

    public enum HRDeviceStates
    {
        NotBonded,
        Disconnected,
        Connected,
        Discovering,
        ServicesFound,
        Transfering,
    }

    public class PolarStatusEventArgs: EventArgs
    {
        public HRDeviceStates newState;
        public BluetoothGatt gattDevice;

        public PolarStatusEventArgs(HRDeviceStates state,BluetoothGatt gatt)
        {
            newState = state;
            gattDevice = gatt;

        }
    }
    public delegate void PolarStatusEventHandler(object sender, PolarStatusEventArgs e);

    public class HRDataEventArgs : EventArgs
    {
        public HRDataEventArgs(HRData data)
        {
            Data = data;
        }
        public HRData Data { get; }
    }

    public class GattStatusEventArgs : EventArgs
    {
        public string msg;
        public GattStatus status;
        public BluetoothGatt gatt;

        public GattStatusEventArgs(string state, GattStatus gstatus, BluetoothGatt btgatt)
        {
            msg = state;
            status = gstatus;
            gatt = btgatt;
        }
    }

    public delegate void HrDataUpdateHandler(object sender, HRDataEventArgs e);

    public delegate void GattStatusEventHandler(object sender, GattStatusEventArgs e);

    public class PolarCallBack : BluetoothGattCallback
    {
        private static readonly NLog.Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public PolarCallBack()
        {

        }
        
        public event HrDataUpdateHandler HRDataUpdate;        
        public event GattStatusEventHandler StatusUpdate;

        protected void OnStatusUpdate(GattStatusEventArgs e)
        {
            if (StatusUpdate != null)
            {
                StatusUpdate(this, e);
            }
        }

        protected void OnHRDataUpdate(HRDataEventArgs e)
        {
            HRDataUpdate?.Invoke(this, e);
        }

        public override void OnConnectionStateChange(BluetoothGatt gatt, [GeneratedEnum] GattStatus status, [GeneratedEnum] ProfileState newState)
        {
            base.OnConnectionStateChange(gatt, status, newState);
            Logger.Debug(string.Format("OnConnectionStateChange: {0} {1}", status.ToString(), newState.ToString()));
            OnStatusUpdate(new GattStatusEventArgs(newState.ToString(), status,gatt));
            

        }
               

        public override void OnPhyRead(BluetoothGatt gatt, [GeneratedEnum] ScanSettingsPhy txPhy, [GeneratedEnum] ScanSettingsPhy rxPhy, [GeneratedEnum] GattStatus status)
        {
            base.OnPhyRead(gatt, txPhy, rxPhy, status);
        }

        public override void OnPhyUpdate(BluetoothGatt gatt, [GeneratedEnum] ScanSettingsPhy txPhy, [GeneratedEnum] ScanSettingsPhy rxPhy, [GeneratedEnum] GattStatus status)
        {
            base.OnPhyUpdate(gatt, txPhy, rxPhy, status);
        }

        public override void OnCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, [GeneratedEnum] GattStatus status)
        {
            base.OnCharacteristicRead(gatt, characteristic, status);

        }
        public override void OnCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, [GeneratedEnum] GattStatus status)
        {
            base.OnCharacteristicWrite(gatt, characteristic, status);
        }

        public override void OnReadRemoteRssi(BluetoothGatt gatt, int rssi, [GeneratedEnum] GattStatus status)
        {
            base.OnReadRemoteRssi(gatt, rssi, status);
        }

        public override void OnServicesDiscovered(BluetoothGatt gatt, [GeneratedEnum] GattStatus status)
        {
            base.OnServicesDiscovered(gatt, status);
            Logger.Debug(string.Format("OnServicesDiscovered: {0}", status));
            OnStatusUpdate(new GattStatusEventArgs("ServicesFound", status, gatt));

        }

        public override void OnCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
        {
            base.OnCharacteristicChanged(gatt, characteristic);
            var data = characteristic.GetValue();

            int hrFormat = data[0] & 0x01;
            bool contactSupported = !((data[0] & 0x06) == 0);
            bool sensorContact = true;

            if (contactSupported)
            {
                sensorContact = ((data[0] & 0x06) >> 1) == 3;
            }
            int energyExpended = (data[0] & 0x08) >> 3;
            int rrPresent = (data[0] & 0x10) >> 4;
            int hrValue = (hrFormat == 1 ? data[1] + (data[2] << 8) : data[1]) & (hrFormat == 1 ? 0x0000FFFF : 0x000000FF);

            if (!contactSupported && hrValue == 0)
            {
                // note does this apply to all sensors, also 3rd party
                sensorContact = false;

            }
            bool sensorContactFinal = sensorContact;
            int offset = hrFormat + 2;
            int energy = 0;
            if (energyExpended == 1)
            {
                energy = (data[offset] & 0xFF) + ((data[offset + 1] & 0xFF) << 8);
                offset += 2;
            }

            List<int> rrs = new List<int>();
            string srrs = "";
            if (rrPresent == 1)
            {
                int len = data.Length;
                while (offset < len)
                {
                    int rrValue = (int)((data[offset] & 0xFF) + ((data[offset + 1] & 0xFF) << 8));
                    offset += 2;
                    rrs.Add(rrValue);
                    srrs += rrValue + " ";
                }
            }
            var now = DateTime.Now;

            //TimeSpan span = last - DateTime.Now;
            //last = DateTime.Now;            

            HRData hrdata = new HRData(now, hrValue, rrs);
            //string result = string.Format("{0}: B:{1} ,HR: {2} MyHR:{3}  rr:{4} ", now, energy, hrValue,hrdata.MyHeartRate, srrs);
            string result = string.Format("{0}: B:{1} ,HR: {2} rr:{3} ", now, energy, hrValue, srrs);
            System.Diagnostics.Debug.Print(result);

            OnHRDataUpdate(new HRDataEventArgs(hrdata));

        }

        public override void OnDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, [GeneratedEnum] GattStatus status)
        {
            base.OnDescriptorRead(gatt, descriptor, status);
        }
        public override void OnDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, [GeneratedEnum] GattStatus status)
        {
            base.OnDescriptorWrite(gatt, descriptor, status);

        }
        public override void OnReliableWriteCompleted(BluetoothGatt gatt, [GeneratedEnum] GattStatus status)
        {
            base.OnReliableWriteCompleted(gatt, status);
        }
        public override void OnMtuChanged(BluetoothGatt gatt, int mtu, [GeneratedEnum] GattStatus status)
        {
            base.OnMtuChanged(gatt, mtu, status);
        }


    }
}